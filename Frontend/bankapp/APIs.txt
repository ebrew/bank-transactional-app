#APIs for linking frontend to backend

#I will change the IP to public IP later since it's dynamic

http://127.0.0.1:5000/signUp
this is a post request which requires accountNumber, fname, lname, phoneNumber, email and password

http://127.0.0.1:5000/login
this is a post request which requires email and password

http://127.0.0.1:5000/deposit
this is a post request which requires accountNumber, type, amount and transactiondate in string

http://127.0.0.1:5000/withdraw
this is a post request which requires accountNumber, type, amount and transactiondate in string


http://127.0.0.1:5000/balance/$accountNumber
this is a get request which requires accountNumber


http://127.0.0.1:5000/ministatement/$accountNumber
this is a get request which requires accountNumber

http://127.0.0.1:5000/reset
this is a post request which requires email and returns a temporary link with a unique token as a response as
http://127.0.0.1:5000/reset/<token> and it's also a post request which requires email and the new password.