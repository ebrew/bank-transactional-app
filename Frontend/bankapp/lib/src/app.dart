import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import './screens/account.dart';
import './screens/sign_up.dart';
import './screens/login.dart';
import './screens/password_recovery.dart';
import './screens/home.dart';
import './screens/deposit.dart';
import './screens/withdraw.dart';
import './screens/statements.dart';
import './screens/balance.dart';
import './screens/transfer.dart';
import './screens/virtual_topup.dart';


class App extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: BaseApp(),

      //Routings
      routes: <String, WidgetBuilder> {
        "/Account" : (context) => Account(),
        "/SignUp" : (context) => SignUp(),
        "/Login" : (context) => Login(),
        "/ResetPassword" : (context) => ResetPassword(),
        "/UserPage" : (context) => UserPage(),
        "/Deposit" : (context) => Deposit(),
        "/Withdraw" : (context) => Withdraw(),
        "/Statements" : (context) => Statements(),
        "/Balance" : (context) => Balance(),
        "/Transfer" : (context) => Transfer(),
        "/VirtualTopup" : (context) => VirtualTopup(),
      },
    );  
  }
}


class BaseApp extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Bank Transactional App',
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        ),
      ), 
      // backgroundColor: Colors.teal,
      body: BackgroundImage(),

      // bottomNavigationBar: Padding(
      //   padding: EdgeInsets.all(10.0),
      //   child: Row(
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     children: <Widget>[
      //       Expanded(
      //         child: RaisedButton(
      //           padding: EdgeInsets.all(15.0),
      //           onPressed: (){
      //             Navigator.pushNamed(context, "/UserPage"); //routes to user screen where no login is required
      //           },
      //           color: Colors.blue,
      //           shape: RoundedRectangleBorder(
      //             borderRadius: BorderRadius.circular(32.0),
      //             side: BorderSide(color: Color(0xFF179CDF))),
      //           child: Text(
      //             "GET STARTED",
      //             style: TextStyle(fontSize: 20.0, color: Colors.white),
      //           ),
      //         )
      //       ),
      //     ],
      //   ),
      // ),
      bottomNavigationBar: CurvedNavigationBar(
        color: Colors.blue,
        backgroundColor: Colors.lightGreen,
        buttonBackgroundColor: Colors.white,
        height: 55,
        items: <Widget>[
          Icon(Icons.person, size:20, color: Colors.black),
          Icon(Icons.home, size:20, color: Colors.black),
          // Icon(Icons.list, size:20, color: Colors.black),
          Icon(Icons.exit_to_app, size:20, color: Colors.black),
        ],
        index: 1, //Default selection
        animationDuration: Duration(
          milliseconds: 50,
        ),
        animationCurve: Curves.bounceInOut,
        onTap: (index){
          switch(index){
            case 1:
              Navigator.pushNamed(context, "/Login");
              break;
            case 2:
              Navigator.pushNamed(context, "/UserPage");
              break;
            default:
              debugPrint('Current Index ontapped id $index');
              // Navigator.pushNamed(context, "/VirtualTopup");
          }
        },
      ),
    );
  }

}

class BackgroundImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(
            child: new Image.asset(
              'assets/images/bkg.PNG',
              width: size.width,
              height: size.height,
              fit: BoxFit.fill,
            ),
          ),
          Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //Epty container to help me position the whole column
              Container(
                margin: EdgeInsets.only(left: 40),
              ),
              CircleAvatar(
                backgroundImage: AssetImage('assets/images/icon.png'),
                radius: 80,
              ),
              Text('KNUST', 
                style: TextStyle(fontSize: 25, 
                  color: Colors.white,
                  fontWeight: FontWeight.bold)
              ),
            ],
          ),
        ]
      )
    ); 
  }
}