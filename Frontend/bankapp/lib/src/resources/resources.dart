import 'dart:async'; //for parallel processing since flutter is a single threaded
import 'dart:convert';
//import 'package:http/http.dart' show Client; //Client is a form of Get but has more features
import 'package:http/http.dart';


final _root = 'http://10.77.103.237:5000'; //private and to shorten length of the APIs and to easily change to  private IP

signupPostRequest (accountNumber,fname, lname, phoneNumber, email, password) async {
  String url = '$_root/signup';
  Map<String, String> headers = {"Content-type": "application/json"};
  String signupData = '{"accountNumber":$accountNumber,"fname":$fname, "lname":$lname, "phoneNumber":$phoneNumber, "email": $email, "password": $password}';
  Response response = await post(url, headers: headers, body: signupData);

  if (response.statusCode == 201) { 
    //Prompt user it's been created
    print('Created!!!');
  } 
  else { 
    throw Exception('Unable to access REST API');
  } 
}

addAccountPostRequest (bankName, accountNumber, branch, accountName, email, phoneNumber) async {
  String url = '$_root/add_account';
  Map<String, String> headers = {"Content-type": "application/json"};
  String data = '{"bankName":$bankName, "accountNumber":$accountNumber,"branch":$branch, "accountName": $accountName, "email": $email, "phoneNumber":$phoneNumber}';
  Response response = await post(url, headers: headers, body: data);

  if (response.statusCode == 201) { 
    //Prompt user it's been created
    print('Created!!!');
  } 
  else { 
    throw Exception('Unable to access REST API');
  } 
}

balanceGetRequest(accountNumber) async {
  String url = '$_root/balance/$accountNumber';
  Response response = await get(url);
  Map<String, String> headers = response.headers;
  String contentType = headers['content-type'];
  if (response.statusCode == 200) {
    final parsedJson =json.decode(response.body);
    return parsedJson;
  } 
  else { 
    throw Exception('Unable to access REST API');
  } 
  
}

statementGetRequest(accountNumber) async {
  String url = '$_root/ministatement/$accountNumber';
  Response response = await get(url);
  Map<String, String> headers = response.headers;
  String contentType = headers['content-type'];
  if (response.statusCode == 200) {
    final parsedJson =json.decode(response.body);
    return parsedJson;
  } 
  else { 
    throw Exception('Unable to access REST API');
  } 
}

loginPostRequest (email, password) async {
  String url = '$_root/login';
  Map<String, String> headers = {"Content-type": "application/json"};
  String loginData = '{"email": $email, "password": $password}';
  Response response = await post(url, headers: headers, body: loginData);
  if (response.statusCode == 200) {
    print('Okay');
    //Direct to the user page and display the welcome response
    // case LoginStatus.cancelledByUser:
    //    _showMessage('Login cancelled by the user.');
    //    break;
    //  case LoginStatus.error:
    //    _showMessage('Something went wrong with the login process.\n'
    //        'Here\'s the error : ${result.errorMessage}');
    //    break;
  } 
  else { 
    throw Exception('Unable to access REST API');
  } 
}

depositPostRequest (accountNumber,type, amount, transactionDate) async {
  String url = '$_root/deposit';
  Map<String, String> headers = {"Content-type": "application/json"};
  String depositData = '{"accountNumber": $accountNumber, "type": $type, "amount": $amount, "transactionDate": $transactionDate}';
  Response response = await post(url, headers: headers, body: depositData);

  if (response.statusCode == 201) { 
    print('Transaction completed');
  } 
  else { 
    throw Exception('Unable to access REST API');
  } 
}

withdrawPostRequest (accountNumber,type, amount, transactionDate) async {
  String url = '$_root/withdraw';
  Map<String, String> headers = {"Content-type": "application/json"};
  String withdrawData = '{"accountNumber": $accountNumber, "type": $type, "amount": $amount, "transactionDate": $transactionDate}';
  Response response = await post(url, headers: headers, body: withdrawData);

  if (response.statusCode == 201) { 
    print('Transaction completed');
  } 
  else { 
    throw Exception('Unable to access REST API');
  } 
}

retrieveAccountRequest (email) async {
  String url = '$_root/reset';
  Map<String, String> headers = {"Content-type": "application/json"};
  String retrieveData = '{"email": $email}';
  Response response = await post(url, headers: headers, body: retrieveData);
  if (response.statusCode == 200) {
    final parsedJson =json.decode(response.body);
    return parsedJson;
  } 
  else { 
    throw Exception('Unable to access REST API');
  } 
}