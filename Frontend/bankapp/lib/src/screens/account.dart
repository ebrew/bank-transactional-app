import 'package:flutter/material.dart';
import './stcked_icons.dart';
import './mixins/validation_mixin.dart';
import '../resources/resources.dart';

class Account extends StatefulWidget{
  @override
  createState (){
    return AccountState();
  }
}

class AccountState extends State<Account> with ValidationMixin{

  final formkey = GlobalKey<FormState>(); //represents the formstate object
  var _banklist = ['Barclays', 'Ghana Commercial Bank', 'Ecobank', 'NIB','MTN Mobile Money', 'Vodafone Cash', 'Others'];
  var bankName = 'Barclays';
  String accountNumber = ''; 
  String branch = '';
  String accountName = '';
  String email = '';  
  String phoneNumber = '';

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text('Add Account Page'),
        backgroundColor: Colors.green[300],
        elevation: 0.0,
        iconTheme: new IconThemeData(color: Colors.blue),
      ),
      body: Center(
        child:ListView(
          shrinkWrap: true,
          padding: EdgeInsets.all(15),
          children: <Widget>[
            Center(
              child: Form(
                key: formkey,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new StackedIcons(),
                    DropdownButton<String>(
                      items: _banklist.map((String dropDownStringItem){
                        return DropdownMenuItem<String>(
                          value: dropDownStringItem,
                          child: Text(dropDownStringItem), //wrapping the item with text
                        );
                      }).toList(), //convert to list
                      hint: Text('Please       choose        the        bank    type'),
                      onChanged: (String newValueSelected){
                        setState(() {
                          this.bankName = newValueSelected;
                        });
                      },
                      value: bankName,
                    ), 
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),                     
                      child: new TextFormField( 
                        decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.account_box),
                          labelText: 'Account Number',
                        ),
                        validator: accountCheck,
                        onSaved: (String value){
                          accountNumber = value;
                        },                     
                      )
                    ),
                    SizedBox(
                      height: 10,
                    ), 
                    Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child: new TextFormField( 
                      decoration: new InputDecoration(
                        labelText: 'Branch or where you opened the account',
                      ),
                      validator: fieldCheck,
                      onSaved: (String value){
                        branch = value;
                      },                      
                    )
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new TextFormField( 
                        decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.person),
                          labelText: 'First Name',
                        ),
                        validator: fieldCheck,
                        onSaved: (String value){
                          accountName = value;
                        },                     
                      )
                    ),
                    SizedBox(
                     height: 10,
                    ),
                    Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child:
                      TextFormField( 
                            keyboardType: TextInputType.emailAddress,
                            decoration: new InputDecoration(
                              prefixIcon: Icon(Icons.contact_mail),
                              labelText: 'Email Address',
                              hintText: 'you@example.com'
                            ),
                      validator: validateEmail,
                      onSaved: (String value){
                        email = value;
                      },                      
                    )
                    ), 
                    new SizedBox(
                     height: 10,
                    ),
                    Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child: new TextFormField( 
                      decoration: new InputDecoration(
                        labelText: 'Telephone Number',
                      ),
                      validator: fieldCheck,
                      onSaved: (String value){
                        phoneNumber = value;
                      },                      
                    )
                    ),
                    new SizedBox(
                     height: 10,
                    ),
                  new SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child: new RaisedButton(
                      color: Colors.blue,
                      child: Text('Submit!'),
                      onPressed: (){
                        if(formkey.currentState.validate()){
                           formkey.currentState.save();
                           addAccountPostRequest (bankName, accountNumber, branch, accountName, email, phoneNumber);
                           formkey.currentState.reset();
                        }
                      },
                    )
                  ),
                ],
              )
            )
          )
        ]
      ) 
    )); 
  }
}