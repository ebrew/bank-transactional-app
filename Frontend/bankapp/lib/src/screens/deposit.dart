import 'package:flutter/material.dart';
import './stcked_icons.dart';
import './mixins/validation_mixin.dart';
import '../resources/resources.dart';

class Deposit extends StatefulWidget{
  @override
  createState (){
    return DepositState();
  }
}

class DepositState extends State<Deposit> with ValidationMixin{

  final formkey = GlobalKey<FormState>(); //represents the formstate object
  String accountNumber = ''; //takes integer and must be converted but gives error when i cast
  var _list = ['Withdrawal', 'Deposit']; //Drop down list
  var type = 'Deposit'; //initial selected value
  String amount = '';  //takes decimal and must be converted but gives error when i cast
  DateTime transactionDate;

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text('Deposit Page'),
        backgroundColor: Colors.green[300],
        elevation: 0.0,
        iconTheme: new IconThemeData(color: Colors.blue),
      ),
      body: Center(
        child:ListView(
          shrinkWrap: true,
          padding: EdgeInsets.all(15),
          children: <Widget>[
            Center(
              child: Form(
                key: formkey,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new StackedIcons(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new TextFormField( 
                        decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.account_box),
                          labelText: 'Account Number',
                        ),
                        validator: accountCheck,
                        onSaved: (String value){
                          accountNumber = value;
                        },                     
                      )
                    ),
                    new SizedBox(
                      height: 10,
                    ), 
                    DropdownButton<String>(
                      items: _list.map((String dropDownStringItem){
                        return DropdownMenuItem<String>(
                          value: dropDownStringItem,
                          child: Text(dropDownStringItem), //wrapping the item with text
                        );
                      }).toList(), //convert to list
                      hint: Text('Please choose the the type of transaction'),
                      onChanged: (String newValueSelected){
                        onDropDownItemSelected(newValueSelected);
                      },
                      value: type,
                    ), 
                    new SizedBox(
                     height: 10,
                    ),
                   Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child: new TextFormField( 
                      decoration: new InputDecoration(
                        labelText: 'Amount to deposit',
                      ),
                      validator: fieldCheck,
                      onSaved: (String value){
                        amount = value;
                      },                      
                    )
                  ),
                  SizedBox(height: 10,),
                  Text(transactionDate == null? 'No date has been picked yet!': transactionDate.toString()),
                  RaisedButton(
                    child: Text('Pick a date for the transaction'),
                    onPressed: (){
                      showDatePicker(
                        context: context,
                        //if date not set then current date is used
                        initialDate: transactionDate == null? DateTime.now():
                        transactionDate,
                        firstDate: DateTime(2020),
                        lastDate: DateTime(2030),
                      ).then((date){
                        setState(() {
                          transactionDate = date;
                        });
                      });
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child: new RaisedButton(
                      color: Colors.blue,
                      child: Text('Submit!'),
                      onPressed: (){
                        if(formkey.currentState.validate()){
                           formkey.currentState.save();
                          //  print('$transactionDate');
                           depositPostRequest (accountNumber,type, amount, transactionDate);
                           formkey.currentState.reset();
                        }
                      },
                    )
                  ),
                ],
              )
            )
          )
        ]
      ) 
    )); 
  }

  void onDropDownItemSelected(String newValueSelected){
    setState(() {
      this.type = newValueSelected;
    });
  }
}
