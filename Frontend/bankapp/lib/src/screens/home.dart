import 'package:flutter/material.dart';
import './stcked_icons.dart';

class UserPage extends StatelessWidget{
  Widget build(context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text('User Home Page'),
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            onPressed: (){
              Navigator.pushNamed(context, "/Account");
            },
            child: Text('Add Account'),
            // shape: CircleBorder(side: BorderSide(color: Colors.transparent)),          
          )
        ],
      ),
      backgroundColor: Colors.teal,
      body: Center(
        child:ListView(
          shrinkWrap: true,
          padding: EdgeInsets.all(70),
          children: <Widget>[
            StackedIcons(),
            SizedBox(height: 20),
            FlatButton(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text('Make Deposit', style: TextStyle(fontSize:20.0, color: Colors.white ),),
              color: Colors.blue,
              onPressed: (){
                Navigator.pushNamed(context, "/Deposit");
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0),
                  side: BorderSide(color: Color(0xFF179CDF))
              ),
            ),
            Container(margin: EdgeInsets.only(top: 30.0)),
            FlatButton(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text('   Make Withdrawal ', style: TextStyle(fontSize:20.0, color: Colors.white )),
              color: Colors.blue,
              onPressed: (){
                Navigator.pushNamed(context, "/Withdraw");
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0),
                  side: BorderSide(color: Color(0xFF179CDF))
              ),
            ),
            Container(margin: EdgeInsets.only(top: 30.0)),
            FlatButton(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text('Request Ministatement', style: TextStyle(fontSize:20.0, color: Colors.white )),
              color: Colors.blue,
              onPressed: (){
                Navigator.pushNamed(context, "/Statements");
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0),
                  side: BorderSide(color: Color(0xFF179CDF))
              ),
            ),
            Container(margin: EdgeInsets.only(top: 30.0)),
            FlatButton(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text('Check Balance', style: TextStyle(fontSize:20.0, color: Colors.white )),
              color: Colors.blue,
              onPressed: (){
                Navigator.pushNamed(context, "/Balance");
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0),
                  side: BorderSide(color: Color(0xFF179CDF))
              ),
            ),
            Container(margin: EdgeInsets.only(top: 30.0)),
            FlatButton(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text(' Logout ', style: TextStyle(fontSize:20.0, color: Colors.white )),
              color: Colors.blue,
              onPressed: (){
                Navigator.pushNamed(context, "/UserScreen"); //Delete the tokens saved in the browser when logged in
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0),
                  side: BorderSide(color: Color(0xFF179CDF))
              ),
            ),
          ],
        ),
      ),

    );
  }
  
}

