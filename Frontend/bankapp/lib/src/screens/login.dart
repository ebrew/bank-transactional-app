import 'package:flutter/material.dart';
import './stcked_icons.dart';
import './mixins/validation_mixin.dart';
import '../resources/resources.dart';

class Login extends StatefulWidget{
  @override
  createState (){
    return LoginState();
  }
}

//This class holds data related to the form
class LoginState extends State<Login> with ValidationMixin{
  final formkey = GlobalKey<FormState>(); //represents the formstate object
  String email = '';
  String password = '';

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text('Home Page', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
        backgroundColor: Colors.green[300],
        elevation: 0.0,
        iconTheme: new IconThemeData(color: Colors.blue),
      ),
      body: Center(
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.all(15.0),
              children: <Widget>[
                StackedIcons(),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Form( 
                     key: formkey,                
                    child: Container(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        children: <Widget>[
                          TextFormField( 
                            keyboardType: TextInputType.emailAddress,
                            decoration: new InputDecoration(
                              prefixIcon: Icon(Icons.contact_mail),
                              labelText: 'Email Address',
                              hintText: 'you@example.com'
                            ),
                            validator: validateEmail,
                            onSaved: (String value){
                              email = value;
                            },
                           ),
                          SizedBox(
                            height: 15.0,
                          ),  
                          TextFormField(
                             obscureText: true ,
                             decoration: new InputDecoration(
                                prefixIcon: Icon(Icons.lock),
                                labelText: 'Password',
                                hintText: 'Password'
                              ),
                             validator: validatePassword,
                             onSaved: (String value){
                               password = value;
                             },
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                            child: new RaisedButton(
                              color: Colors.green[300],
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32.0),
                                side: BorderSide(color: Color(0xFF179CDF))
                              ),
                              child: Text(' Login ', style: TextStyle(fontSize:20.0, color: Colors.white )),
                              onPressed: (){
                                if(formkey.currentState.validate()){
                                  formkey.currentState.save();
                                  loginPostRequest (email, password);
                                  formkey.currentState.reset();                            
                                }
                               },
                             )
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: Padding(
            padding: EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                      padding: EdgeInsets.all(15.0),
                      onPressed: (){
                        Navigator.pushNamed(context, "/SignUp");
                      },
                      color: Colors.green[300],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                            32.0,
                          ),
                          side: BorderSide(color: Color(0xFF179CDF))),
                      child: Text(
                        "Sign Up",
                        style:
                        TextStyle(fontSize: 18.0, color: Colors.white,),
                      ),
                  ),
                  SizedBox(width: 15.0),
                  RaisedButton(
                      padding: EdgeInsets.all(15.0),
                      onPressed: (){
                        Navigator.pushNamed(context, "/ResetPassword"); //routes to user screen where no login is required
                      },
                      color: Colors.green[300],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                            32.0,
                          ),
                          side: BorderSide(color: Color(0xFF179CDF))),
                      child: Text(
                        "Forgot Password",
                        style:
                        TextStyle(fontSize: 18.0, color: Colors.white,),
                      ),
                    ),
                    SizedBox(width: 15.0),
                    RaisedButton(
                      padding: EdgeInsets.all(15.0),
                      onPressed: (){
                        // Navigator.pushNamed(context, "/SignUp");
                      },
                      color: Colors.green[300],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                            32.0,
                          ),
                          side: BorderSide(color: Color(0xFF179CDF))),
                      child: Text(
                        "Get Help",
                        style:
                        TextStyle(fontSize: 18.0, color: Colors.white,),
                      ),
                    ),
              ],
            ),
          ),
        );
      }
    }

