class ValidationMixin {

  String validateEmail(String value){
    if (!value.contains('@gmail.com')){
      return ('Pls enter a valid email address');
    }
    return null;
  }

  String validatePassword(String value){
    if (value.length < 4){
      return ('Password must be at least 4 characters');
    }
    return null;
  }
  
  String accountCheck(String value){
    if (value.length != 8){
      return ('Account number must be 8 integers');
    }
    return null;
  }

  String fieldCheck(String value){
    if (value.length == 0){
      return ('Field can not be empty');
    }
    else if(value.length > 0 && value.length < 4){
      return ('Enter the valid detail');
    }
    return null;
  }
}