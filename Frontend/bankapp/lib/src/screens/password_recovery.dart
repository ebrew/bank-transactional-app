import 'package:flutter/material.dart';
import './stcked_icons.dart';
import './mixins/validation_mixin.dart';
import '../resources/resources.dart';

  
class ResetPassword extends StatefulWidget{
  @override
  createState (){
    return ResetPasswordState();
  }
}

//This class holds data related to the form
class ResetPasswordState extends State<ResetPassword> with ValidationMixin{

  final formkey = GlobalKey<FormState>(); //represents the formstate object
  String email = '';

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text('Password Recovery Page'),
        backgroundColor: Colors.green[300],
        elevation: 0.0,
        iconTheme: new IconThemeData(color: Colors.blue),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 90),
        width: double.infinity,
        child:Form(
              key: formkey,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new StackedIcons(),
                  new Row(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child: new TextFormField( 
                      keyboardType: TextInputType.emailAddress,
                      decoration: new InputDecoration(
                        prefixIcon: Icon(Icons.contact_mail),
                        labelText: 'Email Address',
                        hintText: 'you@example.com'
                      ),
                      validator: validateEmail,
                      onSaved: (String value){
                        email = value;
                      },
                    )
                  ),
                  new SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: new RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32.0),
                        side: BorderSide(color: Color(0xFF179CDF))
                      ),
                      color: Colors.green[300],
                      child: Text('Submit', style: TextStyle(fontSize:20.0, color: Colors.white )),
                      onPressed: (){
                        if(formkey.currentState.validate()){
                           formkey.currentState.save();
                           retrieveAccountRequest (email);
                           formkey.currentState.reset();
                        }
                      },
                    )
                  ),
                ],
              )
            ),
          )
        );
  }
}

