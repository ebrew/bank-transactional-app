import 'package:flutter/material.dart';
import './stcked_icons.dart';
import './mixins/validation_mixin.dart';
import '../resources/resources.dart';

class SignUp extends StatefulWidget{
  @override
  createState (){
    return SignUpState();
  }
}

class SignUpState extends State<SignUp> with ValidationMixin{

  final formkey = GlobalKey<FormState>(); //represents the formstate object
  String accountNumber = ''; //takes integer and must be converted but gives error when i cast
  String fname = '';
  String lname = '';
  String phoneNumber = '';
  String email = '';
  String password = '';

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text('Sign up Page'),
        backgroundColor: Colors.green[300],
        elevation: 0.0,
        iconTheme: new IconThemeData(color: Colors.blue),
      ),
      body: Center(
        child:ListView(
          shrinkWrap: true,
          padding: EdgeInsets.all(15),
          children: <Widget>[
            Center(
              child: Form(
                key: formkey,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new StackedIcons(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new TextFormField( 
                        decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.account_box),
                          labelText: 'Account Number',
                        ),
                        validator: accountCheck,
                        onSaved: (String value){
                          accountNumber = value;
                        },                     
                      )
                    ),
                    new SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new TextFormField( 
                        decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.person),
                          labelText: 'First Name',
                        ),
                        validator: fieldCheck,
                        onSaved: (String value){
                          fname = value;
                        },                     
                      )
                    ),
                    new SizedBox(
                     height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new TextFormField( 
                        decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.person),
                          labelText: 'Last Name',
                        ),
                        validator: fieldCheck,
                        onSaved: (String value){
                          lname = value;
                        },                      
                      )
                    ),
                    new SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new TextFormField( 
                        decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.contact_phone),
                          labelText: 'Phone Number',
                        ),
                        validator: fieldCheck,
                        onSaved: (String value){
                          phoneNumber = value;
                        },
                      )
                    ),
                    new SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new TextFormField( 
                        keyboardType: TextInputType.emailAddress,
                        decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.contact_mail),
                          labelText: 'Email Address',
                          hintText: 'you@example.com'
                        ),
                        validator: validateEmail,
                        onSaved: (String value){
                          email = value;
                        },
                      )
                    ),
                    new SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new TextFormField(
                        obscureText: true, 
                        decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.lock),
                          labelText: 'Password',
                          hintText: 'Password'
                        ),
                        validator: validatePassword,
                        onSaved: (String value){
                          password = value;
                        },
                      )
                    ),
                    new SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new RaisedButton(
                        color: Colors.blue,
                        child: Text('Submit!'),
                        onPressed: (){
                          if(formkey.currentState.validate()){
                             formkey.currentState.save();
                             signupPostRequest (accountNumber,fname, lname, phoneNumber, email, password);
                             formkey.currentState.reset();
                          }
                        },
                      )
                    ),
                  ],
                )
              )
            )
          ]
        ) 
      )
    );
  }
}

