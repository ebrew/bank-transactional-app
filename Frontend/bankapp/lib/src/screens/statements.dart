import 'package:flutter/material.dart';
import './stcked_icons.dart';
import './mixins/validation_mixin.dart';
import '../resources/resources.dart';

class Statements extends StatefulWidget{
  @override
  createState (){
    return StatementsState();
  }
}

//This class holds data related to the form
class StatementsState extends State<Statements> with ValidationMixin{

  final formkey = GlobalKey<FormState>(); //represents the formstate object
  String accountNumber = ''; 

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text('Statement Request Page'),
        backgroundColor: Colors.green[300],
        elevation: 0.0,
        iconTheme: new IconThemeData(color: Colors.blue),
      ),
      body: Container(
        width: double.infinity,
        child:Form(
              key: formkey,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new StackedIcons(),
                  new Row(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child: new TextFormField( 
                      keyboardType: TextInputType.emailAddress,
                      decoration: new InputDecoration(
                        prefixIcon: Icon(Icons.account_box),
                        labelText: 'Account Number'
                      ),
                      validator: accountCheck,
                      onSaved: (String value){
                        accountNumber = value;
                      },
                    )
                  ),
                  new SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child: new RaisedButton(
                      color: Colors.blue,
                      child: Text('Submit', style: TextStyle(fontSize:20.0, color: Colors.white )),
                      onPressed: (){
                        if(formkey.currentState.validate()){
                           formkey.currentState.save();
                           statementGetRequest(accountNumber);
                           formkey.currentState.reset(); //clears the form for a new user
                         }
                      },
                    )
                  ),
                ],
              )
      ),)
    );
  }
}

