import 'package:flutter/material.dart';
import './stcked_icons.dart';
import './mixins/validation_mixin.dart';
import '../resources/resources.dart';

class Transfer extends StatefulWidget{
  @override
  createState (){
    return TransferState();
  }
}

class TransferState extends State<Transfer> with ValidationMixin{

  final formkey = GlobalKey<FormState>(); //represents the formstate object
  var _list = ['Account Transfer',]; //Drop down list
  var type = 'Account Transfer'; //initial selected value
  var _banklist = ['Barclays', 'Ghana Commercial Bank', 'Ecobank', 'NIB','MTN Mobile Money', 'Vodafone Cash', 'Others'];
  var debitbank = 'MTN Mobile Money';
  var creditbank = 'Ecobank';
  String amount = ''; 
  String phoneNumber = ''; 
  DateTime transactionDate;

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text('Transfer'),
        backgroundColor: Colors.green[300],
        elevation: 0.0,
        iconTheme: new IconThemeData(color: Colors.blue),
      ),
      body: Center(
        child:ListView(
          shrinkWrap: true,
          padding: EdgeInsets.all(15),
          children: <Widget>[
            Center(
              child: Form(
                key: formkey,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new StackedIcons(),
                    DropdownButton<String>(
                      items: _list.map((String dropDownStringItem){
                        return DropdownMenuItem<String>(
                          value: dropDownStringItem,
                          child: Text(dropDownStringItem), //wrapping the item with text
                        );
                      }).toList(), //convert to list
                      hint: Text('Please   choose   the the  type   of   transaction'),
                      onChanged: (String newValueSelected){
                        setState(() {
                          this.type = newValueSelected;
                        });
                      },
                      value: type,
                    ),
                    SizedBox(
                     height: 10,
                    ),
                    DropdownButton<String>(
                      items: _banklist.map((String dropDownStringItem){
                        return DropdownMenuItem<String>(
                          value: dropDownStringItem,
                          child: Text(dropDownStringItem), //wrapping the item with text
                        );
                      }).toList(), //convert to list
                      hint: Text('Please   choose    the    account to   debit from '),
                      onChanged: (String newValueSelected){
                        setState(() {
                          this.debitbank = newValueSelected;
                        });
                      },
                      value: debitbank,
                    ),
                    SizedBox(height: 10),
                    DropdownButton<String>(
                      items: _banklist.map((String dropDownStringItem){
                        return DropdownMenuItem<String>(
                          value: dropDownStringItem,
                          child: Text(dropDownStringItem), //wrapping the item with text
                        );
                      }).toList(), //convert to list
                      hint: Text('Please   choose    the    account to   credit to '),
                      onChanged: (String newValueSelected){
                        setState(() {
                          this.creditbank = newValueSelected;
                        });
                      },
                      value: creditbank,
                    ),
                    SizedBox(height: 10),
                   Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child: new TextFormField( 
                      decoration: new InputDecoration(
                        labelText: 'Amount to transfer',
                      ),
                      validator: fieldCheck,
                      onSaved: (String value){
                        amount = value;
                      },                      
                    )
                  ),
                  SizedBox(height: 10),
                  Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: TextFormField( 
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.contact_phone),
                          labelText: 'Phone Number',
                        ),
                        validator: fieldCheck,
                        onSaved: (String value){
                          phoneNumber = value;
                        },
                      )
                    ),
                  new SizedBox(
                    height: 10,
                  ),
                  Text(transactionDate == null? 'No date has been picked yet!': transactionDate.toString()),
                  RaisedButton(
                    child: Text('Pick a date for the transaction'),
                    onPressed: (){
                      showDatePicker(
                        context: context,
                        //if date not set then current date is used
                        initialDate: transactionDate == null? DateTime.now():
                        transactionDate,
                        firstDate: DateTime(2020),
                        lastDate: DateTime(2030),
                      ).then((date){
                        setState(() {
                          transactionDate = date;
                        });
                      });
                    },
                  ),
                  new SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child: new RaisedButton(
                      color: Colors.blue,
                      child: Text('Submit!'),
                      onPressed: (){
                        if(formkey.currentState.validate()){
                           formkey.currentState.save();
                          //  withdrawPostRequest (accountNumber,type, amount, transactionDate);
                           formkey.currentState.reset();
                        }
                      },
                    )
                  ),
                ],
              )
            )
          )
        ]
      ) 
    )); 
  }
}


