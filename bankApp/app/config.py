import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = "postgres://postgres:heaven@localhost:5432/bankApp"
    SECRET_KEY = '@icons@@U%J&36$##$%^%&*^^&%^$#%^&'

    # Advantages of postgres mysql
    # 1. Object relational db while mysql is purely relational db
    # 2. has table inheritance and function overloading
