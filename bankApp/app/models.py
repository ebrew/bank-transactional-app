from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from app import db

Base = declarative_base()


class BaseModel(db.Model):
    __abstract__ = True

    def save(self):
        db.session.add(self)
        db.session.commit()


class CustomerModel(BaseModel):
    __tablename__ = 'customer'

    account_number = db.Column(db.Integer, primary_key=True)
    fname = db.Column(db.String(30))
    lname = db.Column(db.String(20))
    branch = db.Column(db.String(20))
    date_of_birth = db.Column(db.String(13))
    # registration_date = Column(DateTime, default=datetime.datetime.utcnow)
    registration_date = db.Column(db.String(10))
    balance = db.Column(db.Float(precision=2), nullable=False, default=0.00)
    id = db.Column(db.Integer, ForeignKey('account.id'))
    transactions = relationship("TransactionModel", back_populates="customer")
    account = relationship("AccountModel", back_populates="customer")

    def convert_to_json(self):
        customer = {
            'account_number': self.account_number,
            'fname': self.fname,
            'lname': self.lname,
            'branch': self.branch,
            'date_of_birth': self.date_of_birth,
            'registration_date': self.registration_date,
            'balance': self.balance,
            'id': self.id
        }
        return customer

    # customer[registration_date]= datetime.datetime.now()
    # def date_converter(obj):
    #     if isinstance(obj, datetime.datetime):
    #         return obj.__str__()
    #
    # print(json.dumps(customer, default = date_converter))

    @classmethod
    def find_by_account(cls, account_number):
        return cls.query.filter_by(account_number=account_number)


class TransactionModel(BaseModel):
    __tablename__ = 'transaction'

    id = db.Column(db.Integer, primary_key=True)
    account_number = db.Column(db.Integer, ForeignKey('customer.account_number'))
    type = db.Column(db.String(10))
    amount = db.Column(db.Float(precision=2))
    # transaction_date = Column(DateTime, default=datetime.datetime.utcnow)
    transaction_date = db.Column(db.String(10))
    customer = relationship("CustomerModel", back_populates="transactions")

    def convert_to_json(self):
        transaction = {
            'account_number': self.account_number,
            'type': self.type,
            'amount': self.amount,
            'transaction_date': self.transaction_date
        }
        return transaction

    @classmethod
    def find_by_account(cls, account_number):
        return cls.query.filter_by(account_number=account_number)


class AccountModel(BaseModel):
    __tablename__ = 'account'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(40), unique=True)
    password = db.Column(db.String(100))
    token = db.Column(db.String(40), nullable=True)
    # date = Column(db.String(30), nullable=True)
    customer = relationship("CustomerModel", uselist=False, back_populates="account")

    def convert_to_json(self):
        account = {
            'email': self.email,
            'password': self.password,
            'id': self.id
        }
        return account

    @classmethod
    def find_by_token(cls, token):
        return cls.query.filter_by(token=token)

    @classmethod
    def find_by_account(cls, account_number):
        return cls.query.filter_by(account_number=account_number)

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email=email)

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)
