from flask_restful import reqparse

customerParser = reqparse.RequestParser()
customerParser.add_argument(
    'account_number',
    type=int,
    required=True,
    help="This field can't be blank."
)

customerParser.add_argument(
    'fname',
    type=str,
    required=True,
    help="This field can't be blank."
)

customerParser.add_argument(
    'lname',
    type=str,
    required=True,
    help="This field can't be blank."
)

customerParser.add_argument(
    'branch',
    type=str,
    required=True,
    help="This field can't be blank."
)

customerParser.add_argument(
    'date_of_birth',
    type=str,
    required=True,
    help="This field can't be blank."
)

customerParser.add_argument(
    'registration_date',
    type=str,
    required=True,
    help="This field is set to default."
)

customerParser.add_argument(
    'balance',
    type=float,
    required=False,
    help="This field isn't required"
)

customerParser.add_argument(
    'id',
    type=int,
    required=True,
    help="This field can't be blank"
)


transactionParser = reqparse.RequestParser()
transactionParser.add_argument(
    'id',
    type=int,
    required=False,
    help="This field is set to default."
)

transactionParser.add_argument(
    'account_number',
    type=int,
    required=True,
    help="This field can't be blank."
)

transactionParser.add_argument(
    'type',
    type=str,
    required=True,
    help="This field can't be blank: withdrawal or deposit?"
)

transactionParser.add_argument(
    'amount',
    type=float,
    required=True,
    help="This field can't be blank."
)

transactionParser.add_argument(
    'transaction_date',
    type=str,
    required=True,
    help="This field is set to default."
)


accountParser = reqparse.RequestParser()
accountParser.add_argument(
    'id',
    type=int,
    required=False,
    help="This field is set to default."
)

accountParser.add_argument(
    'email',
    type=str,
    required=True,
    help="This field can't be blank."
)

accountParser.add_argument(
    'password',
    type=str,
    required=True,
    help="This field can't be blank."
)

accountParser.add_argument(
    'token',
    type=str,
    required=False,
    help="This field is set to default."
)

# accountParser.add_argument(
#     'date',
#     type=str,
#     required=False,
#     help="This field is set to default."
# )


linkParser = reqparse.RequestParser()
linkParser.add_argument(
    'email',
    type=str,
    required=True,
    help="This field can't be blank."
)
