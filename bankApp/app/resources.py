# The below import is for Login aspects...importing from the library
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token
)
from flask_restful import Resource

from . import status  # The status to import are in the same folder.
from .models import (CustomerModel, TransactionModel, AccountModel)
from .parser import (customerParser, transactionParser, accountParser, linkParser)


class CustomerSignUp(Resource):
    @staticmethod
    def post():
        data = customerParser.parse_args(strict=True)
        customer = CustomerModel()
        customer.account_number = data['account_number']
        customer.fname = data['fname']
        customer.lname = data['lname']
        customer.branch = data['branch']
        customer.date_of_birth = data['date_of_birth']
        customer.registration_date = data['registration_date']
        customer.id = data['id']
        customer.save()
        return customer.convert_to_json(), status.HTTP_201_CREATED


class GetBalance(Resource):
    @staticmethod
    def get(account_number):
        query_set = CustomerModel.find_by_account(account_number)
        customers = []  # To trigger evaluation
        for i in query_set:
            customers.append(i)
        if len(customers) == 0:
            return {"error": "No customer with the entered Account Number exist"}
        customer = customers[0]
        return customer.balance, status.HTTP_200_OK


class Deposit(Resource):
    @staticmethod
    def post():
        data = transactionParser.parse_args(strict=True)
        deposit = TransactionModel()
        deposit.account_number = data['account_number']
        deposit.type = data['type']
        deposit.amount = data['amount']
        deposit.transaction_date = data['transaction_date']
        # balance update
        query_set = CustomerModel.find_by_account(deposit.account_number)
        customers = []  # To trigger evaluation
        for i in query_set:
            customers.append(i)
        if len(customers) == 0:  # TO avoid list index out of range
            return {"error": "No customer with the entered Account Number exist"}
        customer = customers[0]
        customer.balance += deposit.amount
        customer.save()
        deposit.save()
        return deposit.convert_to_json(), status.HTTP_201_CREATED


class Withdraw(Resource):
    @staticmethod
    def post():
        data = transactionParser.parse_args(strict=True)
        withdraw = TransactionModel()
        withdraw.account_number = data['account_number']
        withdraw.type = data['type']
        withdraw.amount = data['amount']
        withdraw.transaction_date = data['transaction_date']
        # balance update
        query_set = CustomerModel.find_by_account(withdraw.account_number)
        customers = []  # To trigger evaluation
        for i in query_set:
            customers.append(i)
        if len(customers) == 0:  # TO avoid list index out of range
            return {"error": "No customer with the entered Account Number exist"}
        customer = customers[0]
        if withdraw.amount > customer.balance:
            return {"error": "Insufficient balance!!!"}
        customer.balance -= withdraw.amount
        customer.save()
        withdraw.save()
        return withdraw.convert_to_json(), status.HTTP_201_CREATED


class Ministatement(Resource):
    @staticmethod
    def get(account_number):
        query_set = TransactionModel.find_by_account(account_number)
        statements = []  # Convert the raw data to list
        for i in query_set:
            statements.append(i.convert_to_json())
        if len(statements) == 0:
            return {"error": "No transaction made for the entered Account Number "}
        return statements, status.HTTP_200_OK


class AddLoginCredentials(Resource):
    @staticmethod
    def post():
        data = accountParser.parse_args(strict=True)
        account = AccountModel()
        account.email = data['email']
        account.password = AccountModel.generate_hash(data['password'])
        account.save()
        return account.convert_to_json(), status.HTTP_201_CREATED


class CustomerLogin(Resource):
    @staticmethod
    def post():
        data = accountParser.parse_args(strict=True)
        query_results = AccountModel.find_by_email(data['email'])
        user = None
        for i in query_results:
            user = i
            break
        if not user:
            return {'message': "User {} doesn't exist".format(data['email'])}

        if AccountModel.verify_hash(data['password'], user.password):
            access_token = create_access_token(identity=user.email)
            refresh_token = create_refresh_token(identity=user.email)
            return {
                'message': 'Logged in as {}'.format(user.email),
                'access_token': access_token,
                'refresh_token': refresh_token
            }

        else:
            return {'message': 'Invalid credentials'}


class LinkGeneration (Resource):
    @staticmethod
    def post():
        data = linkParser.parse_args(strict=True)
        link = AccountModel()
        link.email = data['email']  # Request for email for verification
        query_set = AccountModel.find_by_email(link.email)
        user = None
        for i in query_set:
            user = i
            break
        if not user:
            return {"error": "No customer with the entered email exist"}
        token = create_access_token(identity=user.email)  # Token makes it unique by hiding data
        user.token = token[:20]  # Truncate the first 20 of the token
        user.save()
        print('The below link is only valid for 24hrs!')  # Time to live
        return f'The below link is only valid for 24hrs: http://127.0.0.1:5000/reset/{user.token}'  # String interpolation


class AccountRecovery(Resource):  # This endpoint will be called automatically if temporary link is used
    @staticmethod
    def post(token):
        data = accountParser.parse_args(strict=True)
        recover = AccountModel()
        recover.email = data['email']  # already available
        recover.password = AccountModel.generate_hash(data['password'])  # newly set password
        query_set = AccountModel.find_by_email(recover.email)
        customers = []
        for i in query_set:
            customers.append(i)
        if len(customers) == 0:
            return {"error": "No customer with the entered email exist"}
        customer = customers[0]
        token_verify = AccountModel.find_by_token(token)
        user = None
        for i in token_verify:
            user = i
            break
        if not user:
            return {"error": "Don't try to be smart, Check the link received!"}
        customer.password = recover.password
        customer.save()
        return 'Password reset, Keep ur password safe!!!'
