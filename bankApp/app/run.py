from flask_jwt_extended import JWTManager

from app import app, api, db
from app.resources import (AddLoginCredentials, CustomerSignUp, Deposit, Withdraw, Ministatement,
                           CustomerLogin, GetBalance, AccountRecovery, LinkGeneration)


@app.before_first_request
def create_table():
    db.create_all()


jwt = JWTManager(app)


# Endpoints
api.add_resource(AddLoginCredentials, '/credential')  # Login Account creation1
api.add_resource(CustomerLogin, '/login')  # Customer Login
api.add_resource(CustomerSignUp, '/sign_up')  # 1Sign Up
api.add_resource(Deposit, '/deposit')  # Make a deposit
api.add_resource(Withdraw, '/withdraw')  # Make a withdrawal
api.add_resource(Ministatement, '/ministatement/<account_number>')  # 1 Request Ministatement
api.add_resource(GetBalance, '/balance/<account_number>')  # Check balance
api.add_resource(LinkGeneration, '/reset_link')  # Reset link for Account Recovery
api.add_resource(AccountRecovery, '/reset/<token>')  # Account Recovery

# Logout doesn't need an API: All it needs is to delete
# both the tokens generated from login.

# if __name__ == "__main__":
#     app.run(host='10.24.3.115', port=5000, debug=True)
#     # This tells the operating system to listen on a public IP.

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000, debug=True)
    # local host
